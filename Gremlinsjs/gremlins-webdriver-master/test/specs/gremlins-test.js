function loadScript(callback) {
  var s = document.createElement('script');
  s.src = 'https://rawgithub.com/marmelab/gremlins.js/master/gremlins.min.js';
  if (s.addEventListener) {
    s.addEventListener('load', callback, false);
  } else if (s.readyState) {
    s.onreadystatechange = callback;
  }
  document.body.appendChild(s);
}

function unleashGremlins(ttl, callback) {
  function stop() {
    horde.stop();
    callback();
  }

  //Modify formFiller gremlins.
  var frFGrem = window.gremlins.species.formFiller().canFillElement( function (elem) {
    var selector = 'textarea, input[type="text"], input[type="password"], input[type="number"], input[type="email"], input:not([type])'
    console.log(elem.querySelectorAll(selector).length)
    if (elem.querySelectorAll(selector).length !== 0)
      return true
    return false
  });

  //Modify clicker gremlins.
  var clkGrem = window.gremlins.species.clicker().canClick(function (elem) {
    // console.log(elem.nodeName + " queso")
    if (elem.nodeName === "BUTTON" || elem.nodeName === "A")
      return true;
    return false;
  });

  var horde = window.gremlins.createHorde()
    .gremlin(frFGrem)
      .gremlin(clkGrem)
        .gremlin(window.gremlins.species.toucher())
          .gremlin(window.gremlins.species.scroller())
            .gremlin(window.gremlins.species.typer());

            //Distribución.
  horde.strategy(gremlins.strategies.distribution([0.1,0.4,0.2,0.2,0.2]))
  horde.seed(1234);

  horde.after(callback);
  window.onbeforeunload = stop;
  setTimeout(stop, ttl);
  horde.unleash();
}

describe('Monkey testing with gremlins ', function() {

  it('it should not raise any error', function() {
    browser.url('/');
    browser.click('button=Cerrar');
    // console.log("[Fernando] browser is: " + JSON.stringify(browser));
    // console.log("[Fernando] document is: " + document);
    // console.log("[Fernando] this is: " + JSON.stringify(this));
    // console.log("[Fernado] document has passed.");
    browser.timeoutsAsyncScript(60000);
    browser.executeAsync(loadScript);
    // loadScript(function (){})

    browser.timeoutsAsyncScript(60000);
    browser.executeAsync(unleashGremlins, 50000);
  });

  afterAll(function() {
    browser.log('browser').value.forEach(function(log) {
      browser.logger.info(log.message.split(' ')[2]);
    });
  });

});
