describe('Los estudiantes under monkeys', function() {
    it('visits los estudiantes and survives monkeys', function() {
        cy.visit('https://losestudiantes.co');
        cy.contains('Cerrar').click();
        cy.wait(1000).then(function (){
          // randomClick(10);
          randomEvent(10);
        });
    })
})

function randomClick(monkeysLeft) {
    console.log("Random Click")

    var monkeysLeft = monkeysLeft;
    if(monkeysLeft > 0) {
        cy.get('a').then($links => {
            var randomLink = $links.get(getRandomInt(0, $links.length));
            if(!Cypress.Dom.isHidden(randomLink)) {
                cy.wrap(randomLink).click({force: true});
                monkeysLeft = monkeysLeft - 1;
            }
            setTimeout(randomClick, 1000, monkeysLeft);
        });
    }
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
};

function randomEvent(monkeysLeft){
  if (monkeysLeft > 0) {
    var randomEventNumber = getRandomInt(1, 4);
    switch (randomEventNumber) {
      case 1://Link al azar.
        cy.get('a').then($links => {
          var randomLink = $links.get(getRandomInt(0, $links.length));
          if(!Cypress.Dom.isHidden(randomLink)) {
            cy.wrap(randomLink).click({force: true});
          }
        });
        break;
      case 2://Llenar campo de texto al azar.
        cy.get('input[type="text"]').then($links => {
          var randomLink = $links.get(getRandomInt(0, $links.length));
          if(!Cypress.Dom.isHidden(randomLink)) {
            cy.wrap(randomLink).type("Monkey text.",{force: true});
          }
        });
        break;
      case 3://Seleccionar un combo al azar.
        cy.get('select').then($links => {
          //Handle unwrapped HTMLSelectElement.
          var randomLink = $links.get(getRandomInt(0, $links.length));
          var randomOpt = randomLink.item(getRandomInt(0, randomLink.options.length));
          if(!Cypress.Dom.isHidden(randomLink)) {
            cy.wrap(randomLink).select(randomOpt.value);
          }
        });
        break;
      case 4://Botón al azar.
        cy.get('button').then($links => {
          var randomLink = $links.get(getRandomInt(0, $links.length));
          if(!Cypress.Dom.isHidden(randomLink)) {
            cy.wrap(randomLink).click({force: true});
          }
        });
        break;
    }
    setTimeout(randomEvent, 1000, monkeysLeft - 1);
  }
}
